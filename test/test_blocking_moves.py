import sangaboard
import time

def polling_move(sb, pos):
    positions = []
    sb.move_rel(pos)
    while sb.query("moving?") == "true":
        positions.append((time.time(), sb.position))
        time.sleep(0.001)
    return positions

def blocking_move(sb, pos):
    positions = []
    sb.move_rel(pos)
    sb.query("notify_on_stop")
    return positions

if __name__ == "__main__":
    sb = sangaboard.Sangaboard()

    durations = []
    for i in range(10):
        start = time.time()
        m = polling_move(sb, [1000 * (-1)**i, 0, 0])
        stop = time.time()
        durations.append(stop-start)

    durations = []
    for i in range(10):
        start = time.time()
        m = blocking_move(sb, [1000 * (-1)**i, 0, 0])
        stop = time.time()
        durations.append(stop-start)