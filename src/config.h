#ifndef CONFIGURED
    //board choice
    #define BOARD_AUTO
    //#define BOARD_SANGABOARDv5

    #include <boards.h> //defines wiring

    //general settings
    #define MAX_COMMANDS 50
    #define MAX_MODULES 10
    #define MAX_ARGUMENT_LENGTH 20 //used in argument parsing, takes up ram
    #define VERSION_STRING "Sangaboard Firmware v1.0.0-dev"
    #define DEBUG_ON
    #define EMULATED_EEPROM_SIZE 512

    //module choice
    #define HELP
    #define STAGE
 //   #define ENDSTOPS
 //   #define LIGHT_SENSOR
 //   #define STEPSTICK
    #define ILLUMINATION

    #ifdef ENDSTOPS
        #define ENDSTOPS_MIN
        //#define ENDSTOPS_MAX
        #if defined(ENDSTOPS_MIN) || defined(ENDSTOPS_MAX)
            //endstops are closed when the pin is down  by default, this inverts the behaviour
            //soft endstops trigger when 0/MAX is reached in the direction without endstops
            #define ENDSTOPS_SOFT
        #endif
    #endif

    #define LIGHT_SENSOR_ADAFRUIT_TSL2591
    //#define LIGHT_SENSOR_ADAFRUIT_ADS1115

    #define CONFIGURED
#endif