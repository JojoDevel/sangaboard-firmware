#include "config.h"
#ifdef STEPSTICK
#include "stepstick.h"
#include <ComPort.h>
#ifdef SUPPORT_EEPROM
    #include <EEPROM.h>
#else
    #include "dummyEEPROM.h"
#endif
#include <AccelStepper.h>

//TODO: implement some fancier features of TMC controllers
const uint8_t stepstick_pins[] = WIRING_STEPSTICK;
AccelStepper stepper(AccelStepper::DRIVER, stepstick_pins[1], stepstick_pins[2]);
bool moving = false;

void stepstick_release(String command)
{
    digitalWrite(stepstick_pins[0], HIGH);//start with motor disabled
    comPort->println("Stepstick released");
}

void stepstick_stop(String command)
{
    stepper.stop();
    comPort->println("Stepstick stopping");
}

void stepstick_move(String command)
{
    digitalWrite(stepstick_pins[0], LOW);

    char * args[2];
    parse_arguments(args, command, 2);
    float speed = atof(args[0]);
    long distance = atol(args[1]);
    stepper.setMaxSpeed(speed);
    stepper.moveTo(stepper.currentPosition() + distance);
    free(args[0]);
    free(args[1]);
    comPort->println("Stepstick started");
    moving = true;
}

void stepstick_setup()
{
    pinMode(stepstick_pins[0], OUTPUT);
    digitalWrite(stepstick_pins[0], HIGH);//start with motor disabled
    register_module(stepstick_commands, stepstick_loop);
    stepper.setAcceleration(50000);//256 microsteps
}

void stepstick_loop()
{
    stepper.run();
    if (stepper.distanceToGo() == 0 && moving)
    {
        moving = false;
        comPort->println("Stepstick stopped");
    }
}

extern const Command stepstick_commands[] = {
    {"stepstick_move", stepstick_move},
    {"stepstick_release", stepstick_release},
    {"stepstick_stop", stepstick_stop},
    END_COMMAND};
#endif