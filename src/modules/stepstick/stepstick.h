#include "config.h"
#ifdef STEPSTICK
#ifndef STEPSTICK_H
#include "config.h"
#include "main.h"
#include <Arduino.h>

void stepstick_setup();
void stepstick_loop();

void stepstick_move(String command);
void stepstick_release(String command);
void stepstick_stop(String command);
extern const Command stepstick_commands[];
#define STEPSTICK_H
#endif
#endif