#include "config.h"
#ifdef ILLUMINATION
#ifndef ILLUMINATION_H
#include "config.h"
#include "main.h"
#include <Arduino.h>

void illumination_pwm(String command);
void illumination_pwm_freq(String command);
void illumination_channels(String command);

void cc_set(String command);
void cc_set_value(float val);


void illumination_setup();
extern const Command illumination_commands[];
#define ILLUMINATION_H
#endif
#endif