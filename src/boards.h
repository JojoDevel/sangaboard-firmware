#ifdef BOARD_AUTO
    #ifdef MCU_LEONARDO
        #define BOARD_SANGABOARDv3
    #elif defined(MCU_NANO)
        #define BOARD_SANGABOARDv2
    #elif defined(MCU_BLUEPILL)
        #define BOARD_CUSTOM_BLUEPILL
    #elif defined(MCU_PICO)
        #define BOARD_SANGABOARDv5
    #elif defined(MCU_ESP32)
        #define BOARD_CUSTOM_ESP32
    #elif defined(MCU_UNO)
        #define BOARD_CUSTOM
    #else
        #error "Invalid build config - must specify MCU type"
    #endif
#endif

//TODO: deal with default values better
//Kconfig is probably too complicated
//maybe having a series of #ifndef <option> #def <option> default #endif at the end?
#if defined(BOARD_SANGABOARDv3)
    #define BOARD_STRING "Sangaboard v0.3"
    #define WIRING_MOTOR_X 8, 9, 10, 11
    #define WIRING_MOTOR_Y 5, 13, 4, 12
    #define WIRING_MOTOR_Z 6, 7, A5, A4
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {A0,A1,A2}
    #define WIRING_ENDSTOPS_MAX {A3,A4,A5}
    #define PRIMARY_SERIAL_PORT Serial
    #define PWM_NUM 0
    #define WIRING_PWM_LEDS {}
    #define SUPPORT_EEPROM
    #define STAGE_MAX_MOTORS 3
#elif defined(BOARD_SANGABOARDv2)
    #define BOARD_STRING "Sangaboard v0.2"
    #define WIRING_MOTOR_X 13, 12, 11, 10
    #define WIRING_MOTOR_Y 9, 8, 7, 6
    #define WIRING_MOTOR_Z 5, 4, 3, 2
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {A0,A1,A2}
    #define WIRING_ENDSTOPS_MAX {A3,A4,A5}
    #define PRIMARY_SERIAL_PORT Serial
    #define PWM_NUM 0
    #define WIRING_PWM_LEDS {}
    #define SUPPORT_EEPROM
    #define STAGE_MAX_MOTORS 3
#elif defined(BOARD_CUSTOM_BLUEPILL)
    #define BOARD_STRING "Custom Bluepill board"
    #define WIRING_MOTOR_X PB11, PB10, PB1, PB0
    #define WIRING_MOTOR_Y PA7, PA6, PA5, PA4
    #define WIRING_MOTOR_Z PA3, PA2, PA1, PA0
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {A0,A1,A2}
    #define WIRING_ENDSTOPS_MAX {A3,A4,A5}
    #define PRIMARY_SERIAL_PORT Serial
    #define PWM_NUM 0
    #define WIRING_PWM_LEDS {}
    #define STAGE_MAX_MOTORS 3
#elif defined(BOARD_CUSTOM_PICO)
    #define BOARD_STRING "Custom Pico board"
    #define WIRING_MOTOR_X 2, 3, 4, 5
    #define WIRING_MOTOR_Y 6, 7, 8, 9
    #define WIRING_MOTOR_Z 10, 11, 12, 13
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {A0,A1,A2}
    #define WIRING_ENDSTOPS_MAX {A3,A4,A5}
    #define ADDITIONAL_STEPPERS 1
    #define WIRING_ADDITIONAL_STEPPER 0, 1, 22, 23
    #define WIRING_STEPSTICK {13,14,15} //enable, step, dir todo: move to boards.h
    #define PRIMARY_SERIAL_PORT Serial
    #define SECONDARY_SERIAL_PORT Serial2
    #define PWM_NUM 0
    #define WIRING_PWM_LEDS {}
    #define SUPPORT_EEPROM
    #define STAGE_MAX_MOTORS 4
#elif defined(BOARD_SANGABOARDv5)
    #define BOARD_STRING "Sangaboard v0.5.1"
    #define WIRING_MOTOR_X 18, 19, 20, 21
    #define WIRING_MOTOR_Y 6, 7, 16, 17
    #define WIRING_MOTOR_Z 2, 3, 4, 5
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {26,27,28}
    //#define WIRING_ENDSTOPS_MAX {22,23,0} //not normally used
    #define ADDITIONAL_STEPPERS 1
    #define WIRING_ADDITIONAL_STEPPER 0, 1, 22, 23
    #define WIRING_STEPSTICK {23,22,1} //enable, step, dir todo: move to boards.h
    #define WIRING_SERIAL_TX 8
    #define WIRING_SERIAL_RX 9
    #define PRIMARY_SERIAL_PORT Serial
    #define SECONDARY_SERIAL_PORT Serial2
    #define PWM_NUM 2
    #define WIRING_PWM_LEDS {25, 29}
    #define WIRING_CC_LED 24
    //TODO: 4th motor support
    #define SUPPORT_EEPROM
    #define STAGE_MAX_MOTORS 4
#elif defined(BOARD_CUSTOM_ESP32)
    #define BOARD_STRING "Custom ESP32 board"
    #define WIRING_MOTOR_X 34, 32, 25, 27
    #define WIRING_MOTOR_Y 12, 13, 14, 26
    #define WIRING_MOTOR_Z 23, 35, 18, 5
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {A0,A1,A2}
    #define WIRING_ENDSTOPS_MAX {A3,A4,A5}
    #define WIRING_STEPSTICK {13,12,27}
    #define PRIMARY_SERIAL_PORT Serial
    #define PWM_NUM 0
    #define WIRING_PWM_LEDS {}
    #define STAGE_MAX_MOTORS 3
#elif defined(BOARD_CUSTOM)
    #define BOARD_STRING "Custom UNO board"
    #define WIRING_MOTOR_X 2, 3, 4, 5
    #define WIRING_MOTOR_Y 2, 3, 4, 5
    #define WIRING_MOTOR_Z 2, 3, 4, 5
    #define ENDSTOPS_INVERT false
    #define ENDSTOPS_PULLUPS true
    #define WIRING_ENDSTOPS_MIN {1,2,0}
    //#define WIRING_ENDSTOPS_MAX {22,23,0} //not normally used
    #define ADDITIONAL_STEPPERS 1
    #define WIRING_STEPSTICK {8,9,10} //enable, step, dir todo: move to boards.h
    #define PRIMARY_SERIAL_PORT Serial
    #define PWM_NUM 0
    #define WIRING_PWM_LEDS {}
    #define SUPPORT_EEPROM
    #define STAGE_MAX_MOTORS 3
#endif